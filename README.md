Problem Statement
Implement a HTTP based Queue mirco-service. The service must be scalable. The service offers  queue functions - FIFO over HTTP API adhering to REST semantics. The queue provided is a  durable queue with messages surviving restarts. Queue must adhere to at least once and at most  once delivery. 

Its a Spring Boot based service



Pre-requisite

Java 8

Maria/MySQL DB server


Steps

Create a database named "queue_service".
	e.g. CREATE DATABASE queue_service;

Change database properties in application.properties file.

For first time, to create tables automcatically use following in properties file before starting application
	spring.jpa.hibernate.ddl-auto=create-drop

After creation of tables, you may change to following.
	spring.jpa.hibernate.ddl-auto=update

Compile and Build your project
	e.g. mvn clean install

Copy your jar from target folder to your linux machine or anywhere from where you want to run application

Run following command to start application.

	#In case you want to run with default properties
	java -jar queue-service-1.0.0.jar

	#In case you want to over write properties. For this place your application.properties file along with jar file.
	java -jar -Dspring.config.location=file:application.properties queue-service-1.0.0.jar
	
	



You may use postman collection given in Source folder to execute REST APIs. Before calling an API you may need to modify hostname in URLs.