package com.babbarkrishan.queue.controller;



import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.babbarkrishan.queue.model.Queue;
import com.babbarkrishan.queue.service.MessageService;
import com.babbarkrishan.queue.service.QueueService;
import com.google.gson.Gson;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = QueueController.class)
@WebAppConfiguration
@EnableWebMvc
public class QueueControllerTest {

    protected MockMvc mvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    QueueController queueController;

    @MockBean
    QueueService queueService;
    
    @MockBean 
    private MessageService messageService;
    
    @Test
    public void testCreateQueue() throws Exception {
    	Queue queue = getQueue();
        
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/queue/create";
        
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(this.getJSON(queue))).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals("Getting correct response", 200, status);
    }
    
    @Test
    public void testUpdateQueue() throws Exception {
    	Queue queue = getQueue();
        
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/queue/update";
        
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(this.getJSON(queue))).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals("Getting correct response", 200, status);
    }
    
    /*@Test
    public void testSendCommandPortal() throws Exception {
        Commands commands = new Commands();
        deviceMgmtController.sendPortalCommand(commands);
    }*/
    
    public String getJSON(Queue queue) {
        Gson gson = new Gson();

        String json = gson.toJson(queue);

        return json;
    }
    
    private Queue getQueue() {
    	Queue queue = new Queue();
    	queue.setMaxSize(10);
    	queue.setQueueName("JUnitTestQueue");
    	
        return queue;
    }
}