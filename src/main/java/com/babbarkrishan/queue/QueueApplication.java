package com.babbarkrishan.queue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author Krishan Babbar
 *
 * Startup application 
 */
@SpringBootApplication(scanBasePackages = { "com.babbarkrishan.queue" })
@EnableJpaAuditing
public class QueueApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueueApplication.class, args);
    }
}
