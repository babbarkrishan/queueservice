package com.babbarkrishan.queue.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.babbarkrishan.queue.model.Message;

/**
 * @author Krishan Babbar
 *
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
	
	List<Message> findByQueueIdOrderByCreatedAtAsc(Long queueId);
}
