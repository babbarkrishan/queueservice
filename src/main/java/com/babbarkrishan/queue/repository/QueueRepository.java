package com.babbarkrishan.queue.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.babbarkrishan.queue.model.Queue;

/**
 * @author Krishan Babbar
 *
 */
@Repository
public interface QueueRepository extends JpaRepository<Queue, Long> {
	Optional <Queue> findByQueueName(String qName);
}
