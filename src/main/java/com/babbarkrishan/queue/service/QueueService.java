package com.babbarkrishan.queue.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.babbarkrishan.queue.controller.QueueController;
import com.babbarkrishan.queue.model.Message;
import com.babbarkrishan.queue.model.Queue;
import com.babbarkrishan.queue.repository.MessageRepository;
import com.babbarkrishan.queue.repository.QueueRepository;

/**
 * @author Krishan Babbar
 * 
 * This class is handling CRUD operations of a queue.
 *
 */
@Service
public class QueueService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueueController.class);
    
	@Autowired
	QueueRepository queueRepository;
	
	@Autowired
	private MessageRepository messageRepository;

	public Queue save(Queue queue) {
		return queueRepository.save(queue);
	}
	
	public Optional<Queue> findByQueueName(String qName) {
		return queueRepository.findByQueueName(qName);
	}

	public Optional<Queue> findById(long queueId) {
		return queueRepository.findById(queueId);
	}
	
	public void delete(Queue queue) {
		queueRepository.deleteById(queue.getId());
	}

	public boolean isQueueFull(Queue queue) {
		boolean isFull = true;
		if (queue.getMessages().size() < queue.getMaxSize()) {
			isFull = false;
		}
		return isFull;
	}
	
	public void purgeQueue(Long queueId) {
		Optional<Queue> q = queueRepository.findById(queueId);
		if (q.isPresent()) {
			LOGGER.debug("Purging queue: {}", queueId);
			for (Message msg : q.get().getMessages() ) {
				messageRepository.delete(msg);
			}
			//messageRepository.deleteAll(q.get().getMessages());
		}
		else {
			LOGGER.debug("Purging stopped as queue is not available: {}", queueId);
		}
	}
}
