package com.babbarkrishan.queue.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.babbarkrishan.queue.model.Message;
import com.babbarkrishan.queue.repository.MessageRepository;

/**
 * @author Krishan Babbar
 * This class is handling message operations within a queue 
 */
@Service
public class MessageService {

	@Autowired
	private MessageRepository messageRepository;

	public Message save(Message message) {
		return messageRepository.save(message);
	}
	
	public Optional<Message> findById(long messageId) {
		return messageRepository.findById(messageId);
	}
	
	public void delete(Message message) {
		messageRepository.deleteById(message.getId());
	}

	public Message dequeue(Long queueId) {
		Message msg = null;
		List<Message> messages = messageRepository.findByQueueIdOrderByCreatedAtAsc(queueId);
		if (messages.size() > 0) {
			msg = messages.get(0);
			messageRepository.deleteById(msg.getId());
		}
		return msg;
	}
	
	public Message peek(Long queueId) {
		Message msg = null;
		List<Message> messages = messageRepository.findByQueueIdOrderByCreatedAtAsc(queueId);
		if (messages.size() > 0) {
			msg = messages.get(0);
		}
		return msg;
	}
}
