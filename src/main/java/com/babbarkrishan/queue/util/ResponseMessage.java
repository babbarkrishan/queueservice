package com.babbarkrishan.queue.util;


/**
 * @author Krishan Babbar
 *
 */
public class ResponseMessage {

	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String STATUS = "status";
	public static final String MESSAGE = "message";
	
	public static final String QUEUE = "queue";
	public static final String QUEUE_MESSAGE = "queueMessage";
	
	public static final String QUEUE_ADDED_SUCCESS = "Queue has been saved successfully.";
	public static final String QUEUE_UPDATED_SUCCESS = "Queue has been updated successfully.";
	public static final String QUEUE_DELETED_SUCCESS = "Queue has been deleted successfully.";
	public static final String QUEUE_PURGED_SUCCESS = "Queue has been purged successfully.";
		
	public static final String QUEUE_ALREADY_EXISTS = "Queue name already exists.";
	public static final String QUEUE_DOES_NOT_EXIST = "Given queue does not exists.";
	public static final String QUEUE_IS_FULL = "Given queue is full.";
	
	public static final String QUEUE_CREATE_FAILED = "Unable to create queue.";
	public static final String QUEUE_UPDATE_FAILED = "Unable to update queue.";
	

	public static final String MESSAGE_ADDED_SUCCESS = "Message has been added in queue successfully.";
	public static final String QUEUE_IS_EMPTY = "Queue is empty.";
}
