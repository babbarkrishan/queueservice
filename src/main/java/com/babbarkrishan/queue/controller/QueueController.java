package com.babbarkrishan.queue.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.babbarkrishan.queue.model.Message;
import com.babbarkrishan.queue.model.Queue;
import com.babbarkrishan.queue.service.MessageService;
import com.babbarkrishan.queue.service.QueueService;
import com.babbarkrishan.queue.util.ResponseMessage;

/**
 * @author Krishan Babbar
 * 
 *	This class has exposed APIs for CRUD operation of Queue and other operations for processing messages in queue 
 *
 */
@RestController
@RequestMapping("/queue")
public class QueueController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueueController.class);

    @Autowired 
    private QueueService queueService;
    
    @Autowired 
    private MessageService messageService;

    /*
     * Create Queue Operation
     */
	@PostMapping(path = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> createQueue(@RequestBody Queue queue) {
		LOGGER.info("createQueue: {}", queue.toString());
		Map<String, Object> response = new HashMap<>(2);
		
		if (queueService.findByQueueName(queue.getQueueName().trim()).isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_ALREADY_EXISTS);
		}
		else {
			queueService.save(queue);
			
			response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_ADDED_SUCCESS + " Queue Id: " + queue.getId());
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/*
     * Update existing Queue Operation
     */
	@PutMapping(path = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateQueue(@RequestBody Queue queue) {
		LOGGER.info("updateQueue: {}", queue.toString());
		Map<String, Object> response = new HashMap<>(2);
		
		Optional<Queue> queueOpt = queueService.findById(queue.getId());
		
		if (!queueOpt.isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DOES_NOT_EXIST);
		}
		else {
		
			Queue qToBeUpdated = queueOpt.get();
			
			boolean isError = false;
			if (!queue.getQueueName().equals(qToBeUpdated.getQueueName())) {
				if (queueService.findByQueueName(queue.getQueueName().trim()).isPresent()) {
					response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
					response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_ALREADY_EXISTS);
					isError = true;
				}
			}
		
			if (!isError) {
				qToBeUpdated.setMaxSize(queue.getMaxSize());
				qToBeUpdated.setQueueName(queue.getQueueName());
			
				queueService.save(qToBeUpdated);
				
				response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
				response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_UPDATED_SUCCESS);
			}
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/*
     * Delete a Queue Operation
     */
	@DeleteMapping(path = "/delete/{queue-id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteQueue(@PathVariable(value = "queue-id") Long queueId) {
		LOGGER.info("deleteQueue: {}", queueId);
		
		Map<String, Object> response = new HashMap<>(2);
		
		Optional<Queue> queueOpt = queueService.findById(queueId);
		if (!queueOpt.isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DOES_NOT_EXIST);
		}
		else {
			queueService.delete(queueOpt.get());
	
			response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DELETED_SUCCESS);
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/*
     * Get a Queue Operation
     */
	@GetMapping(path = "/get/{queue-id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getQueue(@PathVariable(value = "queue-id") Long queueId) {
		LOGGER.info("getQueue: {}", queueId);
		Map<String, Object> response = new HashMap<>(2);
		
		Optional<Queue> queueOpt = queueService.findById(queueId);
		if (!queueOpt.isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DOES_NOT_EXIST);
		}
		else {
			response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
			response.put(ResponseMessage.QUEUE, queueOpt.get());
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	/*
     * Enqueue a message in a Queue Operation
     */	
	@PostMapping(path = "/{queue-id}/enqueue", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> enqueue(@PathVariable(value = "queue-id") Long queueId, @RequestBody Message message) {
		LOGGER.info("enqueue: Queue Id: {}, Message: {}", queueId, message.toString());
		Map<String, Object> response = new HashMap<>(2);
		
		Optional<Queue> queueOpt = queueService.findById(queueId);
		if (!queueOpt.isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DOES_NOT_EXIST);
		}
		else  if (queueService.isQueueFull(queueOpt.get())) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_IS_FULL);
		} else {
			Queue queue = queueOpt.get();
			message.setQueue(queue);
			
			messageService.save(message);
			
			response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.MESSAGE_ADDED_SUCCESS);
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/*
     * Dequeue a message from Queue Operation
     */
	@DeleteMapping(path = "/{queue-id}/dequeue", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> dequeue(@PathVariable(value = "queue-id") Long queueId) {
		LOGGER.info("dequeue: Queue Id: {}", queueId);
		Map<String, Object> response = new HashMap<>(2);
		
		Optional<Queue> queueOpt = queueService.findById(queueId);
		if (!queueOpt.isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DOES_NOT_EXIST);
		}
		else {	
			Message message = messageService.dequeue(queueId);
			
			if (message == null) {
				response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
				response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_IS_EMPTY);
			}
			else {
				response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
				response.put(ResponseMessage.QUEUE_MESSAGE, message);
			}
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/*
     * Purge Queue Operation
     */
	@DeleteMapping(path = "/{queue-id}/purge", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> purgeQueue(@PathVariable(value = "queue-id") Long queueId) {
		LOGGER.info("purgeQueue: Queue Id: {}", queueId);
		Map<String, Object> response = new HashMap<>(2);
		
		Optional<Queue> queueOpt = queueService.findById(queueId);
		if (!queueOpt.isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DOES_NOT_EXIST);
		}
		else {	
			queueService.purgeQueue(queueId);
			response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_PURGED_SUCCESS);
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	/*
     * Peek Queue Operation
     */
	@GetMapping(path = "/{queue-id}/peek", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> peekMessage(@PathVariable(value = "queue-id") Long queueId) {
		LOGGER.info("peekMessage: Queue Id: {}", queueId);
		Map<String, Object> response = new HashMap<>(2);
		
		Optional<Queue> queueOpt = queueService.findById(queueId);
		if (!queueOpt.isPresent()) {
			response.put(ResponseMessage.STATUS, ResponseMessage.ERROR);
			response.put(ResponseMessage.MESSAGE, ResponseMessage.QUEUE_DOES_NOT_EXIST);
		}
		else {	
			Message message = messageService.peek(queueId);
			
			response.put(ResponseMessage.STATUS, ResponseMessage.SUCCESS);
			response.put(ResponseMessage.QUEUE_MESSAGE, message);
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}