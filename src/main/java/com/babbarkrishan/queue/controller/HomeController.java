package com.babbarkrishan.queue.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Krishan Babbar
 * Welcome Page
 */
@RestController
@RequestMapping("/")
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @GetMapping(value = "/hi")
    public ResponseEntity<String> welcome() {
    	LOGGER.info("Entering welcome()....");

        return new ResponseEntity<String>("Hey Krishan!! Welcome", HttpStatus.OK);
    }
}